<?php
require "../conexion.php";
require "../common.php";

if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $connection = new PDO($dsn, $usuario, $contraseña);

    $new_user = array(
      "apellido_paterno" => $_POST['apellido_paterno'],
      "apellido_materno"  => $_POST['apellido_materno'],
      "nombres"  => $_POST['nombres'],
      "email"     => $_POST['email'],
      "edad"       => $_POST['edad'],
      "procedencia"  => $_POST['procedencia']
    );

    $sql = sprintf(
      "INSERT INTO %s (%s) values (%s)",
      "usuarios",
      implode(", ", array_keys($new_user)),
      ":" . implode(", :", array_keys($new_user))
    );

    $statement = $connection->prepare($sql);
    $statement->execute($new_user);
  } catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}
?>
<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
  <blockquote><?php echo escape($_POST['apellido_paterno']); ?> Agregado exitosamente.</blockquote>
<?php endif; ?>
<body>
<h2>Agregar Usuario</h2>

<form method="post">
  <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>"><dr></dr>
  <label for="apellido_paterni">Apellido Paterno</label>
  <input type="text" name="apellido_paterno" id="apellido_paterno">
  <br><br>
  <label for="apellido_materno">Apellido Materno</label>
  <input type="text" name="apellido_materno" id="apellido_materno">
  <br><br>
  <label for="nombres">Nombres</label>
  <input type="text" name="nombres" id="nombres">
  <br><br>
  <label for="email">Email</label>
  <input type="text" name="email" id="email">
  <br><br>
  <label for="edad">Edad</label>
  <input type="text" name="edad" id="edad">
  <br><br>
  <label for="procedencia">Procedencia</label>
  <input type="text" name="procedencia" id="procedencia">
  <br><br>
  <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Regresar</a>

<style>
    
    body{
       text-align: center; 
       color: #00008B;   
       background-image: url("https://img.freepik.com/foto-gratis/fondo-acuarela-pintada-mano-forma-cielo-nubes_24972-1095.jpg?w=900&t=st=1684268052~exp=1684268652~hmac=975f9e38f0be2256739a9d33a073e8ecb1aa133205824076d8c281349f0502e7");
       background-repeat: no-repeat;
       background-size: cover;
    }
    input{
        margin-left: 20px;
        color: #00008B;
        background:#F8F8FF;
        border-radius: 5px;
    }
    a{
       color: 00008B; 
       text-decoration: none;
    }

    form{
        background-color: #B0C4DE;
        padding: 15px;
        width: 20%;
        margin: auto;
        border-radius: 5px;

    }
  
</style>
</body>
<?php require "templates/footer.php"; ?>


