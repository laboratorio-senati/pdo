<?php

require "../conexion.php";
require "../common.php";

$success = null;

if (isset($_POST["submit"])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);
  
    $id = $_POST["submit"];

    $sql = "DELETE FROM users WHERE id = :id";

    $statement = $conexion->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $success = "Usuario eliminado correctamente";
  } catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}

try {
  $conexion = new PDO($dsn, $usuario, $contraseña);

  $sql = "SELECT * FROM usuarios";

  $statement = $conexion->prepare($sql);
  $statement->execute();

  $result = $statement->fetchAll();
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>
<?php require "templates/header.php"; ?>
        
<h2>Eliminar Usuarios</h2>

<?php if ($success) echo $success; ?>
<body>
<form method="post">
  <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
  <table>
    <thead>
      <tr>
        <th>#</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Nombres</th>
        <th>Email</th>
        <th>Edad</th>
        <th>Procedencia</th>
        <th>Fecha</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($result as $row) : ?>
      <tr>
        <td><?php echo escape($row["id"]); ?></td>
        <td><?php echo escape($row["apellido_paterno"]); ?></td>
        <td><?php echo escape($row["apellido_materno"]); ?></td>
        <td><?php echo escape($row["nombres"]); ?></td>
        <td><?php echo escape($row["email"]); ?></td>
        <td><?php echo escape($row["edad"]); ?></td>
        <td><?php echo escape($row["procedencia"]); ?></td>
        <td><?php echo escape($row["fecha"]); ?> </td>
        <td><button type="submit" name="submit" value="<?php echo escape($row["id"]); ?>">Eliminar</button></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
</form>

<a href="index.php">Regresar al inicio</a>

<style>
  body{
    background-image: url("https://static.vecteezy.com/system/resources/previews/004/697/688/original/curve-light-blue-background-abstract-free-vector.jpg");
    background-repeat: no-repeat;
    background-size: cover;
    text-align: center;
  }

 table{
  text-align: left;
  width: 50%;
  margin: auto;
  padding: 30px;

 }
</style>
  
</body>

<?php require "templates/footer.php"; ?>
